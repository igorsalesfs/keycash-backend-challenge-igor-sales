import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import configService from './config/config.services';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HttpExceptionFilter } from './commons/filters/http-exception-filter.filter';
import { PropertyModule } from './app/property/property.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      autoLoadEntities: true,
      ...configService.getTypeOrmConfig(),
    }),
    PropertyModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: APP_FILTER, useClass: HttpExceptionFilter },
  ],
})
export class AppModule {}
