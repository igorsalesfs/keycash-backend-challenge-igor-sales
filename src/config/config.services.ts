import { TypeOrmModuleOptions } from '@nestjs/typeorm';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'mysql',
      host: process.env.DB_HOST,
      port: 3306,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: false,
      logging: true,
      //logger: 'file',
      migrationsTableName: 'migrations-node',
      migrations: [__dirname + '/../db/migrations/*{.ts,.js}'],
      cli: {
        migrationsDir: 'src/db/migrations',
      },
    };
  }
}

const configService = new ConfigService(process.env);

export default configService;
