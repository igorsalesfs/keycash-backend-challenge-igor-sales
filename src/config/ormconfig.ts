import configService from './config.services';

export default configService.getTypeOrmConfig();
