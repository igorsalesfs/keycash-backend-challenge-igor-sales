import { Controller } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import {
  Post,
  Body,
  Get,
  Put,
  Delete,
  Param,
  HttpException,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { FilterProperty, PropertyService } from './property.service';
import { Property } from './property.entity';
import { CreatePropertyDto } from './dto/create-property.dto';
import { UpdatePropertyDto } from './dto/update-property.dto';
import { QueryFilterPropertyDto } from './dto/query-filter-property.dto';

@ApiTags('properties')
@Controller('properties')
export class PropertyController {
  constructor(protected service: PropertyService) {}

  @ApiOperation({ summary: 'Requests all properties' })
  @ApiResponse({
    status: 200,
    type: Property,
    isArray: true,
  })
  @ApiQuery({ type: QueryFilterPropertyDto, required: false })
  @Get()
  allData(@Query() query: FilterProperty): Promise<Property[]> {
    return this.service.all(query);
  }

  @ApiOperation({ summary: 'Requests a property' })
  @ApiResponse({
    status: 200,
    type: Property,
  })
  @Get(':id')
  async get(@Param() params): Promise<Property> {
    await this._checkExists(params.id);
    return await this.service.find(params.id);
  }

  @ApiOperation({ summary: 'Creates a property' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: Property,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Post()
  async create(@Body() object: CreatePropertyDto) {
    return await this.service.create(object);
  }

  @ApiOperation({ summary: 'Updates a property' })
  @ApiResponse({
    status: 200,
    type: Property,
  })
  @ApiParam({
    name: 'id',
    required: true,
    description: 'either an integer for the property id',
    schema: { oneOf: [{ type: 'integer' }] },
  })
  @Put(':id')
  async update(@Param() params, @Body() object: UpdatePropertyDto) {
    await this._checkExists(params.id);
    return this.service.update(params.id, object);
  }

  @ApiOperation({ summary: 'Deletes a property' })
  @ApiResponse({
    status: 200,
  })
  @ApiParam({
    name: 'id',
    required: true,
    description: 'either an integer for the property id',
    schema: { oneOf: [{ type: 'integer' }] },
  })
  @Delete(':id')
  async delete(@Param() params) {
    await this._checkExists(params.id);
    return this.service.delete(params.id);
  }

  protected async _checkExists(_id: number): Promise<any> {
    const data = await this.service._checkExists(_id);

    if (!data) {
      throw new HttpException(
        {
          message: `ID not found (${this.constructor.name})`,
          statusCode: HttpStatus.NOT_FOUND,
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      return true;
    }
  }
}
