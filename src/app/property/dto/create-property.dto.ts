import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreatePropertyDto {
  @ApiProperty()
  @IsNotEmpty()
  sqmetres: number;

  @ApiProperty()
  @IsNotEmpty()
  barthrooms: boolean;

  @ApiProperty()
  @IsNotEmpty()
  bedrooms: number;

  @ApiProperty()
  @IsNotEmpty()
  parkingSpaces: number;

  @ApiProperty()
  @IsNotEmpty()
  state: string;

  @ApiProperty()
  @IsNotEmpty()
  city: string;

  @ApiProperty()
  street: string;

  @ApiProperty()
  @IsNotEmpty()
  price: string;
}
