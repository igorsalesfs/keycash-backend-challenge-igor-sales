import { ApiProperty } from '@nestjs/swagger';

export class QueryFilterPropertyDto {
  @ApiProperty({ required: false })
  sqmetres: number;

  @ApiProperty({ required: false })
  bedrooms: number;

  @ApiProperty({ required: false })
  parkingSpaces: number;
}
