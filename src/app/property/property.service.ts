import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base/base.service';
import { Repository } from 'typeorm';
import { Property } from './property.entity';

export interface FilterProperty {
  sqmetres?: number;
  bedrooms?: number;
  parkingSpaces?: number;
}

@Injectable()
export class PropertyService extends BaseService<Property> {
  constructor(
    @InjectRepository(Property)
    private service: Repository<Property>,
  ) {
    super(service);
  }

  async find(_id: number): Promise<any> {
    const where = { id: _id };

    return await this.service.findOne({ where: where });
  }

  async all(
    filter: FilterProperty = {
      bedrooms: null,
      parkingSpaces: null,
      sqmetres: null,
    },
  ): Promise<Property[]> {
    return await this.service.find(filter);
  }
}
