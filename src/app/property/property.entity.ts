import { Base } from 'src/base/base.entity';
import { Column, Entity } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity('properties')
export class Property extends Base {
  @ApiProperty()
  @Column()
  sqmetres: number;

  @ApiProperty()
  @Column()
  barthrooms: number;

  @ApiProperty()
  @Column()
  bedrooms: number;

  @ApiProperty()
  @Column()
  parkingSpaces: number;

  @ApiProperty()
  @Column()
  state: string;

  @ApiProperty()
  @Column()
  city: string;

  @ApiProperty()
  @Column({ nullable: true })
  street: string;

  @ApiProperty()
  @Column()
  price: string;
}
