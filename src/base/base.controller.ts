import {
  Post,
  Body,
  Get,
  Put,
  Delete,
  Param,
  HttpException,
  HttpStatus,
  Req,
  Query,
} from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Options } from './base.service';

export class BaseController {
  constructor(protected service: any) {}

  @ApiOperation({ summary: 'Requests all rows' })
  @ApiResponse({
    status: 200,
  })
  @Get()
  allData(
    @Req() req: any,
    @Query() query: Options = { qtd: null },
  ): Promise<any[]> {
    return this.service.all(query);
  }

  @ApiOperation({ summary: 'Requests a row' })
  @ApiResponse({
    status: 200,
  })
  @Get(':id')
  async get(@Param() params): Promise<any> {
    await this._checkExists(params.id);

    return await this.service.find(params.id);
  }

  @ApiOperation({ summary: 'Creates a row' })
  @ApiResponse({
    status: 200,
  })
  @Post()
  async create(@Body() object: any) {
    return await this.service.create(object);
  }

  @ApiOperation({ summary: 'Updates a row' })
  @ApiResponse({
    status: 200,
  })
  @Put(':id')
  async update(@Param() params, @Body() object: any) {
    await this._checkExists(params.id);
    return this.service.update(params.id, object);
  }

  @ApiOperation({ summary: 'Deletes a row' })
  @ApiResponse({
    status: 200,
  })
  @Delete(':id')
  async delete(@Param() params) {
    await this._checkExists(params.id);
    return this.service.delete(params.id);
  }

  protected async _checkExists(_id: number): Promise<any> {
    const data = await this.service._checkExists(_id);

    if (!data) {
      throw new HttpException(
        {
          message: `ID not found (${this.constructor.name})`,
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      return true;
    }
  }
}
