import { Repository } from 'typeorm';

export interface Options {
  qtd?: number;
}

export class BaseService<EntityType> {
  constructor(protected repository: Repository<any>) {}

  async all(): Promise<any[]> {
    return await this.repository.find();
  }

  async find(_id: number): Promise<any> {
    const where = { id: _id };

    return await this.repository.findOne({ where: where });
  }

  async create(object: any) {
    return this.repository.save(object);
  }

  async update(_id: number, object: any) {
    await this.repository.update(_id, object);
    return this.find(_id);
  }

  async delete(_id: number) {
    return await this.repository.delete(_id);
  }

  async findByName(name: string): Promise<any> {
    const where = { name: name };

    return await this.repository.findOne({ where: where });
  }

  async _checkExists(_id: number): Promise<any> {
    const data = await this.find(_id);

    //no id found
    if (!data) {
      return false;
    }

    return data;
  }
}
