
## Description

Api Challenge Keycash

## Installation

```bash
$ npm install
```
### * Renomear arquivo .env.example para .env e definir variáveis de acordo com as da máquina local
## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Document

http://localhost:3000/docs/

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

